# nlp_book_code
    code in nlp book

# naming method 命名方式
    document serial number corresponds to book chapters 文件序号对应书本章节
    the code file name is named according to the sequence number appearing in the chapter文件里的代码文件名按照章节中出现的顺序编号命名，比如2-3为第二章第三段代码

# functions of code files 各代码文件功能
    1
        1-1 building LSTM Model with Keras 应用Keras搭建LSTM模型
    2
        2-1 the call of LR in sklearn sklearn中逻辑回归的调用
        2-2 the call of Naive Bayes in sklearn sklearn中朴素贝叶斯的调用
        2-3 the call of Decision Tree in sklearn sklearn中决策树的调用
        2-4 solving PCA by Gradient Rising Method 应用梯度上升法求解PCA
        2-5 application of PCA in sklearn sklearn中PCA的应用
    4
        4-1 capital to lowercase 大写字母转小写
        4-2 digital processing 数字处理
        4-3 punctuation symbol processing 标点符号处理
        4-4 blank treatment 空白处理
        4-5 stemming 词干提取
        4-6 word form restoration 词形还原
        4-7 POS tagging 词性标注
        4-8 Named Entity Recognition 命名实体识别
        4-9 phrase extraction 词组提取
        4-10 StanfordCoreNLP segmentation StanfordCoreNLP分词
        4-11 HanLP segmentation HanLP分词
        4-12 THULAC segmentation THULAC分词
        4-13 SnowNLP segmentation SnowNLP分词
        4-14 jieba segmentation jieba分词
        4-15 english spelling correction 英文拼写纠错
        4-16 TextRank keyword extraction TextRank关键词提取
        4-17 random sampling 随机方法采样
        4-18 SMOTE sampling SMOTE方法采样
    5
        5-1 word representation using gensim 利用gensim进行词表征
        5-2 word representation using sklearn 利用sklearn进行词表征
        5-3 training w2v using skip-gram model skip-gram训练词向量
    6
        6-1 weather prediction using hmmlearn 利用hmmlearn预测天气
    9
        9-1 text classification using Decision Tree应用决策树进行文本分类
        9-2 text classification using Naive Bayes应用朴素贝叶斯进行文本分类
        9-3 text classification using Multilayer Perceptron 应用多层感知机进行文本分类
    10
        10-1 summary generation based on word frequency statistics 基于词频统计的摘要生成
        10-2 summary generation based on graph model 基于图模型的摘要生成
    11
        11-1 IBM model1 establish statical model by EM algorithm IBM model1 利用EM建立统计模型
    12
        12-1 chatbot 闲聊机器人
    13
        13-1 sum of two number 两数之和
        13-2 longest substring 最长子串
        13-3 bracket matching problem  括号问题
        13-4 edit distance 编辑距离
        13-5 balanced tree 平衡树
